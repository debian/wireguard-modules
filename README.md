Wireguard binary kernel modules for Debian
------------------------------------------

The goal of this debian source package is to facilitate the creation
and distribution of binary Linux kernel modules for wireguard popular
Debian architectures.

That is, it should produce a binary package for each architecture that
installs wireguard.ko and invokes depmod appropriately, without the
user needing to have dkms or a compiler installed.  It should also
include a metapackage that ensures that apt upgrade works smoothly and
reliably.

The current plan is to start with the amd64 platform and manually
issue prompt new releases of the source package to debian unstable
that handle kernel ABI changes and new wireguard versions.

This source package itself should not include any wireguard source,
but should instead build-depend on wireguard-dkms and the appropriate
linux-headers packages.

The binary module packages produced should be co-installable by kernel
ABI, just as different kernel packages are co-installable.

Each arch-specific metapackage should manage dependency relationships
so that a system with both the metapackage and the architecture's
kernel metapackage installed will always have a wireguard kernel
module available for the latest debian kernel.

See the [discussion on the APT mailing
list](https://lists.debian.org/deity/2019/09/msg00017.html) for more
background.
